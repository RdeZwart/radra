//DOM ready
window.addEventListener('load', init);

var map, searchForm, select, optionValue, finalDestination,finalLocation ;

//search bar
var searchForm = document.getElementById('search');

/**
 * Initialize application
 */
function init(){


    //option
    var optionValue = document.getElementById('option');

    //search on keyup
    searchForm.addEventListener('keyup', searchLocation);

}

function initMap(){

    //map options
    var mapOptions = {
        center: {lat: 52.191735, lng: 3.0369282},
        styles: [
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#7f2200"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#87ae79"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#495421"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "visibility": "on"
                    },
                    {
                        "weight": 4.1
                    }
                ]
            },
            {
                "featureType": "administrative.neighborhood",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#abce83"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "lightness": "-44"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    },
                    {
                        "saturation": "-100"
                    },
                    {
                        "lightness": "-100"
                    },
                    {
                        "gamma": "0.19"
                    },
                    {
                        "weight": "0.01"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#769E72"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#7B8758"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#EBF4A4"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "simplified"
                    },
                    {
                        "color": "#8dab68"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#5B5B3F"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#ABCE83"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#EBF4A4"
                    },
                    {
                        "invert_lightness": true
                    },
                    {
                        "weight": "0.01"
                    },
                    {
                        "lightness": "66"
                    },
                    {
                        "saturation": "-46"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#9BBF72"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#A4C67D"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#aee2e0"
                    }
                ]
            }
        ],
        zoom: 8,
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        zoomControl: false,
        disableDefaultUI: true
    };

    //create a map object and specify the DOM element for display.
    map = new google.maps.Map(document.getElementById('map'), mapOptions);

}

function searchLocation(data, finalDestination){

    var location = $(this).val();

    //check if current field is empty
    if (location === "") {
        // Empty select
        $(select).empty();
        // Hide empty select
        $(select).hide();
    } else{

        //get the location if correct
        var searchLocation = './api.php/location/search/?query=' + location;
        //console.log(searchLocation);

        $.getJSON(searchLocation)
        .done(loadInformation)
        .fail(failed);


    }



}

function loadInformation(data){



    //console log the current array
    //console.log(data);

    //retrive alertbox
    alertBox = document.getElementById('alertBox');

    if (data.length > 10) {

        //Empty the select
        $(select).empty();

        //Remove any previous warnings
        $(alertBox).remove();

        //Only show 10 results instead of 100+
        var arrayLength = 10;

    } else if (data.length === 0) {

        //Remove any previous warnings
        $(alertBox).remove();

        //Create alert and fill it with a warning message
        alert = document.createElement('div');
        $(alert).attr('id', 'alertBox');
        $(alert).attr('class', 'alert alert-danger');
        message = document.createTextNode('No information available for this location! Enter another city.');
        alert.appendChild(message);
        document.body.appendChild(alert);

    } else if (data.length == 1) { 

        //Display all results from array and show the select
        var arrayLength = data.length;

        //Get woeid
        var finalDestination = data[0].woeid;

        loadDestination(finalDestination);

        return finalDestination;

    } else {        

        //Remove any previous warnings
        $(alertBox).remove();

        //Display all results from array and show the select
        var arrayLength = data.length;

    }
    
    var availablePlaces = [];


    // Options
    for (i = 0; i < arrayLength; i++) {

        //Console log the current city's
        console.log(data[i].title);

        //Push specific information into an Array
        availablePlaces.push(data[i].title);

    }

    //Read the array and show available places 
    $( "#search" ).autocomplete({
        source: availablePlaces
    });

}

function loadDestination(finalDestination){
    //console.log(finalDestination); 


    //get the location if correct
    var getLocation = './api.php/location/' + finalDestination;
    //console.log(getLocation);

    $.getJSON(getLocation)
    .done(initWeather)
    .fail(failed);
}

function initWeather(data){

    //Add marker to page
    var latFormat = data.latt_long;
    var latFormatted = latFormat.split(",");
    var iconContent = data.consolidated_weather[0].weather_state_abbr; 
    console.log(data.consolidated_weather[0].weather_state_abbr);

    //Get correct image
    var icon = { 
        url: 'https://www.metaweather.com/static/img/weather/png/64/' + iconContent + '.png'
    };

    //Replace arrow with image
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng( parseFloat(latFormatted[0]), parseFloat(latFormatted[1]) ),
        map: map,
        icon: icon
    });

    var sidebar = document.getElementById("mySidenav");


   if ($(sidebar).html().length > 0) {
    $(sidebar).empty();
   }   

    //Add sidebar title 
    var h = document.createElement("h2");
    var title = document.createTextNode("Weather overview ");
    h.appendChild(title);
    sidebar.appendChild(h);

    //Fill sidebar with information
    for (var i = 0; i < 3; i++) {
        console.log(data.consolidated_weather[i].weather_state_abbr);

        //Create container
        var div = document.createElement("div");
        $(div).attr('id', 'box');


        if (i == 0) {
            var day = "<h2>Today</h2>";
        } else if (i == 1) {
            var day = "<h2>Tomorrow</h2>";
        } else {
            var date = new Date(data.consolidated_weather[i].applicable_date);
            var days = new Array(7);
            days[1] = "Monday";
            days[2] = "Tuesday";
            days[3] = "Wednesday";
            days[4] = "Thursday";
            days[5] = "Friday";
            days[6] = "Saturday";
            days[7] = "Sunday";
            var r = days[date.getDay()];
            var day = "<h2>" + r + "</h2>";
        }

        //Display the date 
        var title = "<h3>" + data.consolidated_weather[i].applicable_date + "</h3>";

        //Display weather image
        var image = "<img src='https://www.metaweather.com/static/img/weather/png/64/" + data.consolidated_weather[i].weather_state_abbr + ".png'>";

        //Display weather in text
        var kind =  "<h3>" + data.consolidated_weather[i].weather_state_name + "</h3>";

        //Weather info
        var temp = Math.round(data.consolidated_weather[i].the_temp);

        var num = data.consolidated_weather[i].wind_speed;
        var wind = Math.round(num * 100) / 100;

        var info = "<ul><li> " + temp + "</li><li> " + wind + "</li><li> " + data.consolidated_weather[i].humidity + "</li></ul>";

        var content = day + title + image + kind + info; 
        $(div).append(content);

        sidebar.appendChild(div);



    }

    openNav();

    //Move center off the map to the marker
    map.setCenter({lat:parseFloat(latFormatted[0]), lng:parseFloat(latFormatted[1])});

    //console.log(map);
    marker.setMap(map);



}

/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "350px";
    document.getElementById("mySidenav").style.padding = "30px";
    document.getElementById("map").style.marginRight = "350px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("mySidenav").style.padding = "0";
    document.getElementById("main").style.marginLeft = "0";
}



function failed(){
    console.log('Something is not right with the json call!');
}
